=======
SaltLab
=======

SaltLab is a tool to manage local experimental Salt clusters, suitable for
experimentation, development, and testing.

Installation
------------
.. code-block:: bash

    pip install saltlab

Usage
-----

To start your cluster, run:

.. code-block:: bash

    saltlab init

Spin up a minion with:

.. code-block:: bash

    saltlab spawn myminion

Mount your SLS files with:

.. code-block:: bash

    saltlab mount ~/my/state/directory base:

Destroy everything when you are done:

.. code-block:: bash

    saltlab nuke

See all the commands with:

.. code-block:: bash

    saltlab --help

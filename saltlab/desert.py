import camel
import docker
from pathlib import Path
from .cluster import Cluster
from .client import Container, MasterContainer
from .di import registry

__all__ = 'load_savefile', 'save_savefile', 'clear_savefile'

FILENAME = '.saltlab'

cregistry = camel.CamelRegistry()


@cregistry.dumper(Container, 'container', version=1)
@cregistry.dumper(MasterContainer, 'master', version=1)
@cregistry.dumper(docker.models.containers.Container, 'container', version=1)
def _dump_container(cont):
    return cont.id


@cregistry.loader('container', version=1)
def _load_container(data, version):
    return Container.get(data)


@cregistry.loader('master', version=1)
def _load_master(data, version):
    return MasterContainer.get(data)


@cregistry.dumper(docker.models.networks.Network, 'network', version=1)
def _dump_network(net):
    return net.id


@cregistry.loader('network', version=1)
def _load_network(data, version):
    client = registry['DockerClient']
    return client.networks.get(data)


@cregistry.dumper(docker.models.images.Image, 'image', version=1)
def _dump_image(img):
    return img.id


@cregistry.loader('image', version=1)
def _load_image(data, version):
    client = registry['DockerClient']
    return client.images.get(data)


@cregistry.dumper(Cluster, 'cluster', version=1)
def _dump_cluster(obj):
    return {
        'master': obj.master,
        'network': obj.network,
        'minions': obj.minions,
    }


@cregistry.loader('cluster', version=1)
def _load_cluster(data, version):
    return Cluster(data['master'], data['network'], data['minions'])


def _potential_directories(cwd):
    projdir = cwd
    while projdir != projdir.parent:
        yield projdir
        projdir = projdir.parent

    yield Path.home()


def find_savefile(*, cwd=...):
    if cwd is ...:
        cwd = Path.cwd()

    for projdir in _potential_directories(cwd):
        if (projdir / FILENAME).exists():
            break
    else:
        projdir = cwd

    return projdir / FILENAME


def load_savefile(*, cwd=...):
    fn = find_savefile(cwd=cwd)
    camel = registry['Camel']
    if fn.exists():
        cluster = camel.load(fn.read_text())
        cluster.source_file = str(fn)
        return cluster


def save_savefile(obj, *, new=False, cwd=...):
    fn = find_savefile(cwd=cwd)
    camel = registry['Camel']
    fn.write_text(camel.dump(obj))


def clear_savefile(*, cwd=...):
    fn = find_savefile(cwd=cwd)
    if fn.exists():
        fn.unlink()
